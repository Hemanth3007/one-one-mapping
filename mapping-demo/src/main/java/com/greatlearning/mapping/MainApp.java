package com.greatlearning.mapping;

import com.greatlearning.mapping.dao.InstructorDao;
import com.greatlearning.mapping.entity.Instructor;
import com.greatlearning.mapping.entity.InstructorDetail;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Instructor instructor = new Instructor("Krishna", "Gupta", "krishna@gmail.com");

        InstructorDetail instructorDetail = new InstructorDetail("http://www.youtube.com", "Guitar");
        // associate the objects
        instructorDetail.setInstructor(instructor);
        // associate the objects
        instructor.setInstructorDetail(instructorDetail);

        // when you get instructorDetail then hibernate also saves instructor info
        InstructorDao instructorDao = new InstructorDao();
        instructorDao.saveInstructor(instructor);

	}

}
